Overview:

solver is a shell written to explore the structure of solutions to the point vortex
equilibrium problem with equal strength vortices using clifford algebra, or Geometric Algebra.
This program was used to find general conditions for the solutions, test geometric derivatives,
and explore conjectures about patterns within solutions, by rotating an N-1 dimensional simplex
in R^(N-1).

Although solver is very much a work in progress there is enough data in the archive for an interested
reader to search for, verify and load solutions into a simple visualization pipeline.

The git archive will be open to contributors!

A few sample workflows are provided below.

-Phillip
robertson.phillip@gmail.com


Software Dependencies:

scipy
numpy
bokeh
clifford

ELEMENTARY WORKFLOWS:

(loading and displaying solutions)
> load filename.p
> graph

(adding more solutions to the list of stored solutions)
> load_append filename.p
> graph

(searching for a single 2d solution)
> search_2d <number of particles>
> plot

(if it is a solution you are interested in)
> store
> save filename.p

(to display the list of currently stored solutions)
> graph

(to change the objective function)
> objective_function <function_name>

(currently, only two objective functions are used to search for solutions:
'energy' and 'bivector_equation'

(while calculating a bivector-optimized solution, trace the calculation)
> trace




