#!/usr/bin/python3
#####################################################################
# Simplex: Classes for finding relative equilibrium solutions
# of point vortexes through rotations of a regular N-1 dimensional
# simplex.
#
# Author: Phillip Robertson (c) 2022.
#####################################################################

import clifford as cf
import clifford.tools as tools
import scipy.optimize as optimize
from bokeh.plotting import figure, output_file, show
from bokeh.layouts import gridplot
from bokeh.palettes import magma
from datetime import datetime
from clifford import tools
from itertools import combinations
import numpy as np
import random
import pickle
import copy

kH = 1 / (4 * np.pi)
kPSEUDO_ZERO = 1 / 1000000.0
kTHRESHHOLD = 1.0 ** (-10)

class Solution:
    
    def __init__(self, N, energy, error, vertex_list, theta_list, projection,
                 rotor, objective_function, optimizer, projected_dim, geometry): 
        self.N = N
        self.energy = energy
        self.error = error
        self.vertex_list = vertex_list
        self.theta_list = theta_list
        self.projection = projection
        self.rotor = rotor
        self.objective_function = objective_function
        self.optimizer = optimizer
        self.projected_dim = projected_dim
        self.timestamp = datetime.timestamp(datetime.now())
        self.geometry = geometry
        self.result = None
        self.bounds = None

        
class Geometry:
    
    def __init__(self, dimension, projected_dim):
        self.dimension = dimension
        self.layout, self.blades = cf.Cl(dimension)
        self.N = self.dimension + 1
        self.basis_vectors = self.b_vectors()
        self.two_blades = self.two_blades()
        self.projected_dim = projected_dim
        self.vertex_len = self.vertex_length()

    def b_vectors(self):
        # create basis vector list.
        b_v = []
        for i in range(1, self.N):
            b_v.append(self.blades[self.layout.names[i]])
        return b_v

    def two_blades(self):
        # create a set of all 2-blades for the geometry.
        blades = []
        for i in range(self.N,
                       int(self.dimension * (self.dimension - 1)/2 + (self.N))):
            blades.append(self.blades[self.layout.names[i]])
        return blades

    def pseudoscalar(self):
        return(self.blades[self.layout.names[2**(self.N-2)]])

    def vertex_length(self):
        if self.projected_dim == 1:
            return self.dimension/np.sqrt(2)
        elif self.projected_dim == 2:
            return self.dimension/2


class Solver:

    def __init__(self, dimension, vertex_len, projected_dim):
        self.projected_dim = projected_dim
        self.geometry = Geometry(dimension, projected_dim)
        self.num_indep_angles = 2*dimension - 4
        self.bounds = self.calculate_bounds(dimension, self.projected_dim)
        self.simplex = Simplex(vertex_len, self.geometry)
        self.random_simplex = None
        self.N = self.geometry.N
        self.dimension = dimension
        self.trace = False
        self.outputfile = "plots.html"
        self.objective_function = self.energy
        self.maxiter = 1000
        self.solution_candidate = None
        self.solution_list = []
        self.graph_list = []
        self.generated_list = []
        self.transform_graph_list = []
        self.outputfile = output_file(self.outputfile)
        self.delta = 0.005
        self.num_cols = 4
        
    def bivector_equation(self, projected_dim):
        s = 0
        for i in range(0, self.N):
            t = 0
            for j in range(0, self.N):
                if  j != i:
                    edge_rotated = self.simplex.simplex_rotated[i] - \
                        self.simplex.simplex_rotated[j]
                    edge_projected = self.simplex.project_vector(edge_rotated, projected_dim)
                    edge_rejected = self.simplex.reject_vector(edge_rotated, projected_dim)
                    edge_magnitude_s = abs(edge_projected) ** 2
                    t = (edge_projected/edge_magnitude_s)
                    s += edge_rejected^t
            if self.trace:
                print('bivector: {} abs {}:'.format(s, abs(s)))
        return(abs(s))

    def create_random_simplex(self, verlen, geom):
        simplex = Simplex(verlen, geom)
        g = [random.uniform(-1,1) for i in range(0, (2 * (geom.N -1)) - 4)]
        angles = np.array(g)
        
        simplex.rotate_simplex(angles, geom.projected_dim)
        simplex.project_simplex(geom.projected_dim)        

        random_simplex_solution = Solution(geom.N, 0, 0,
                                           simplex.simplex_rotated,
                                           angles,                                           
                                           simplex.simplex_projected_2d,
                                           simplex.rotation.get_rotor(),
                                           'random_simplex',
                                           'random_simplex',
                                           geom.projected_dim,
                                           geom
        )
        print('create_random_simplex(): simplex created')
        self.solution_list.append(random_simplex_solution)

    def sinc(self, x):
        return np.sin(x)/x

    def log_rotor(self, V):
        """
        Logarithm of a simple rotor
        """
        if (V(2)**2).grades() != {0}:
            print(V)
        # raise ValueError('Bivector is not a Blade.')
        # (but it doesn't do that, does it? - pr)
        if abs(V(2)) < cf.eps():
            return np.log(float(V(0)))
        
        # numpy's trig correctly chooses hyperbolic or not with Complex args
        theta = np.arccos(complex(V(0)))
        return V(2)/self.sinc(theta).real

    def solution_blades(self, s):
        """Expand bivector equation and compare terms with other solutions."""
        print('\nsolution: {}'.format(self.solution_list.index(s)+1))

        e12 = s.geometry.two_blades[0]

        blade_projection_sum = 0
        blade_sum = 0
        bivector_terms = []
        projectors = []
        tan2_sum = 0
        for i in range(0, s.N):
            v = s.vertex_list[i]
            edge_inv_square_sum = 0
            for j in range(0, s.N):
                w = s.vertex_list[j]
                if j >  i:
                    projected_edge = self.project(v-w, e12)
                    projectors.append(projected_edge)
                    rejected_edge = self.reject(v-w, e12)
                    edge = v-w
                    tan2_sum += rejected_edge**2/(projected_edge**2)
                    edge_inv_square_sum += 1/projected_edge**2
                    term = rejected_edge ^ (projected_edge).inv()
                    # filter zero blades
                    
                    bivector_terms.append(term)
                    blade_sum += rejected_edge ^ (projected_edge.inv())

        #print('E: {} exp(-4Pi*E): {}'.format(s.energy, np.e**(-4*np.pi*s.energy)))
        return bivector_terms    
            
    def run(self):
        if len(self.solution_list) != 0:
            root_solution = self.solution_list[0]
        else:
            print('test(): No seed solution found. Exiting.')
            return

        for s in self.solution_list:
           bivector_terms = self.solution_blades(s)
           angle_dict = self.blade_angles(bivector_terms, s)
           self.traverser(angle_dict)

    def blade_angles(self, blade_list, solution):
        angle_list = []
        ref_blade = solution.geometry.two_blades[1]
        for i in range(0, len(blade_list)):
                if abs(blade_list[i]) > 0.01:
                    scalar_product =  ref_blade | (~blade_list[i])
                    cos_theta = scalar_product/(abs(blade_list[0]) * abs(blade_list[i]))
                    if abs(cos_theta) > 1.0:
                        # normalize
                        cos_theta = cos_theta/abs(cos_theta)

                    theta = np.arccos(float(cos_theta))                    
                    angle_list.append(theta)
        
        return(angle_list)

    def blade_angle_dict(self, blade_list):
        print('length(blade_list): {}'.format(len(blade_list)))
        blade_d = {}
        for i in range(0, len(blade_list)):
            for j in range(0, len(blade_list)):
                if j>i:
                    scalar_product = blade_list[i] | ~(blade_list[j])
                    blade_d[(i, j)] = float(scalar_product)/(abs(blade_list[i]) * abs(blade_list[j]))
        return blade_d                                
        
    def construct_rotor(self, theta, blade):
        t = np.e ** (-(theta/2)*blade) 
        return(t)    
                
    def project(self, v, b):
        p = (v | b) * b.inv()
        return p

    def reject(self, v, b):
        p = (v ^ b)*b.inv()
        return p
    
    def sum_proj_squared_lengths(self):
        for solution in self.solution_list:
            for k in range(1, solution.geometry.N + 1):
                # pick out all combinations of length k.
                c = combinations(solution.projection, k)
                for t in c:
                    s = 0
                    for v in t:
                        s += abs(v) ** 2
                    print('sol: {} vertices: {} sum: {}'.format(self.solution_list.index(solution), k, s))
                print()
            print()

        N = self.solution_list[0].geometry.N 
        s = 0 
        for vertex in self.solution_list[0].projection:
            s+=abs(vertex)**2
        s = 4*s/(N-1)**2
        print('proj sum square: ', s)
                
    def plot_eigenvectors(self, eigen_list):
        print('plotting eigenvalues')
        eig_graph = figure(title='eigenvalue plot')
        ex = []
        ey = []
        for e in eigen_list:
            print('poly:')
            print(np.poly(e))
            for f in e:
                ex.append(np.real(f))
                ey.append(np.imag(f))
        eig_graph.scatter(ex, ey)
        show(eig_graph)

    def factorial(self, n):
        return np.prod(range(1, n+1))

    def get_blades(self, solution, k):
        v_list = combinations(solution.vertex_list, k)
        b_list = []
        for v in v_list:
            v_wedge = 1
            for e in v:
                v_wedge = v_wedge ^ e
            b_list.append(v_wedge)
        return b_list
    
    def commutator(self, a, b):
        return ((1/2) * ((a*b) - (b*a)))

    def rotation_factor(self, steps, solution, rotor):
        step_rotor = 1
        for i in range(1, steps+1):
            step_rotor = np.e ** ((-np.pi/steps) * i * solution.geometry.two_blades[0])
            print(step_rotor, ' ', step_rotor * rotor *(~step_rotor))

    def convert_to_matrix(self, rotor, solution):
        basis_vectors = solution.geometry.basis_vectors
      
        # Let rotor act on an orthonormal frame.
        print('matrix conversion\n')
        frame = [rotor * e * ~rotor for e in basis_vectors]
        mat_list = []
        for v in frame:
            row = []
            for e in basis_vectors:
                row.append(float(v|e))
            mat_list.append(row)
            
        M = np.array(mat_list)
        return M

    def project_vector2(self, vector, solution):
        e1 = solution.geometry.basis_vectors[0]
        e2 = solution.geometry.basis_vectors[1]
        return((vector | e1) * e1 + (vector | e2) * e2)

    def project_blade(self, blade, solution):
        e12 = solution.geometry.two_blades[0]
        # print('project_blade: ', blade)
        return((blade | e12) | (e12.inv()))

    def reject_blade(self, blade, solution):
        e12 = solution.geometry.two_blades[0]
        # print('reject_blade: ', blade ^ e12)
        return((blade ^ e12) * (e12.inv()))

    def calculate_bounds(self, dimension, projected_dim):
        if projected_dim == 1:
            bounds = [(-1, 1) for i in range(0, dimension - 1)]
        elif projected_dim == 2:
            bounds = [(-1, 1) for i in range(0, self.num_indep_angles)] 
        return tuple(bounds)
 
    def calculate_guess(self, projected_dim):        
        g = []
        dimension = self.dimension
        if projected_dim == 1:
            for i in range(0, dimension - 1):
                g.append(random.uniform(-1, 1))
        elif projected_dim == 2:
            for i in range(0, self.num_indep_angles):
                g.append(random.uniform(-1, 1))            
        return np.array(g)

    def get_projection(self, projected_dim):       
        if projected_dim == 1:
            projection = self.simplex.simplex_projected_1d
        elif projected_dim == 2:
            projection = self.simplex.simplex_projected_2d
        return projection
    
    def energy(self, projected_dim):       
        s = 1.0
        projection = self.get_projection(projected_dim)
        for i in range(0, self.N):
            for j in range(0, self.N):
                if i != j:
                    s = s * (1 / abs(projection[i] - projection[j]))
        return kH * np.log(s)

    def morton_sum(self, i, solution):       
        e1 = solution.geometry.basis_vectors[0]
        s = 0

        projection = solution.projection
        for j in range(0, solution.N):
            if j != i:
                if abs(projection[i] - projection[j]) > kPSEUDO_ZERO:
                    s = s + (projection[i] - projection[j]).inv()
                else:
                    s = e1*(1/kPSEUDO_ZERO)
        return s

    def morton_vectors(self, solution):       
        mv = [self.morton_sum(i, solution) for i in range(0, solution.N)]
        return mv
    
    def bivector_projection(self, projected_dim):      
        s = 0
        for i in range(0, self.N):
            s += self.simplex.reject_vector(self.simplex.simplex_rotated[i], projected_dim) ^\
                self.simplex.project_vector(self.simplex.simplex_rotated[i], projected_dim)

    def drop(self):
        self.solution_list = []

    def least_squares(self, solution):
        w = 0
        projection = solution.projection    
        for i in range(0, solution.N):
            inner = projection[i] | self.morton_sum(i, solution)
            square = self.morton_sum(i, solution) * (~self.morton_sum(i, solution))
            proj_square = abs(projection[i]) ** 2
            t = abs(projection[i] - self.morton_sum(i, solution)) ** 2
            w += t
        return w

    def plot(self):
        if self.solution_candidate != None:
            show(self.make_graph(self.solution_candidate))
        else:
            print('plot(): No solution candidate in buffer. Run \'search_2d <arg>\'')
    def make_graph(self, solution):
        errorstr = f'{solution.error:.3e}'
        energystr = f'{solution.energy:.4e}'
        titlestr = "N="+str(solution.N)+"      "\
            +"minimize: "+solution.objective_function+"       error="+errorstr
        graph = figure(title=titlestr)
        graph.xgrid.visible = False
        graph.ygrid.visible = False
        e1 = solution.geometry.basis_vectors[0]
        e2 = solution.geometry.basis_vectors[1]
        validate = False

        # TODO: set enviroment variable that plots error/morton points.
        if solution.projected_dim == 2: 
            x = [float(v | e1) for v in solution.projection]
            y = [float(v | e2) for v in solution.projection]
            # Display 'morton points' which should coincide with projected vertices.
            # when they do not exactly coincide, the points are displaced from their
            # equilibirum positions. 
            try:
                mv = self.morton_vectors(solution)
                mx = [float(v | e1) for v in mv]
                my = [float(v | e2) for v in mv]
            except:
                print('solver: failed to find morton points')
                return
                
            graph.scatter(x, y)
            graph.scatter(mx, my, color='red')
            
        elif solution.projected_dim == 1:
            x = [float(v | e1) for v in solution.projection]            
            y = [0 for v in solution.projection]
            graph.scatter(x, y)

        self.graph_list.append(graph)
        return(graph)

    def make_edge_theta_dict(self, solution):
        e12 = solution.geometry.two_blades[0]
        
        edge_theta_dict = {}

        for i in range(0, solution.N):
            for j in range(0, solution.N):
                if j>i:
                    edge_len = abs(solution.vertex_list[i] - solution.vertex_list[j])
                    cos_theta = abs(self.project(solution.vertex_list[i]-solution.vertex_list[j], e12))/edge_len
                    edge_theta = np.arccos(cos_theta)
                    edge_theta_dict[(i,j)]= edge_theta
        return edge_theta_dict
    
    """ Compute scalar product of sum of the bivector tangent blades. """ 
    def edge_theta_equation(self, edge_theta_dict, solution):
        edge_len2 = (solution.N-1) * (solution.N)/2
        edge_theta = 0
        e12 = solution.geometry.two_blades[0]
                
    def make_projector_graph(self, solution):
        titlestr = "projectors"

        # projectors graph
        graph = figure(title=titlestr)
        e1 = solution.geometry.basis_vectors[0]
        e2 = solution.geometry.basis_vectors[1]
        projectors = []
        print('solution N: {}'.format(solution.N))
        for i in range(0, solution.N):
            for j in range(0, solution.N):
                if j > i:
                    projector = solution.projection[i] - solution.projection[j]
                    projectors.append(projector)

        for p in solution.projection:
            print(p)
                    
        if solution.projected_dim == 2: 
            x = [float(v | e1) for v in projectors]
            y = [float(v | e2) for v in projectors]

        graph.scatter(x, y, color = 'red')
        self.graph_list.append(graph)
        # end projectors graph

        # edge_angles graph
        bivector_terms = self.solution_blades(solution)
        
        edge_thetas = []
        # blade magnitudes are tan(theta_ij), where theta_ij is the edge angle with e12. 
        for bv in bivector_terms:
            edge_thetas.append(np.arctan(np.sqrt(float(bv | (~bv)))))
            # print('theta: {}'.format(np.arctan(float(bv/abs(bv)))))
            
        graph2 = figure(title = '\u03B8\u2097\u2098')
        edge_thetas.sort(key=float)
        edge_thetas_index = [i for i in range(0, len(edge_thetas))]
        ea_sum = 0
        for e in edge_thetas:
            ea_sum += e

        tan2_sum = 0
        for f in bivector_terms:
            tan2_sum += abs(f)**2
            
        graph2.line(edge_thetas_index, edge_thetas)
        self.graph_list.append(graph2)

        #### end edge_angles graph ####

        #### blade scalar product graph
        b_d = self.blade_angle_dict(bivector_terms)
        blade_angle_list = [e for e in b_d.values()]
        blade_angle_list.sort(key=float)
                    
        graph4 = figure(title = 'B\u2097 * B\u2098')
        x_a = [i for i in range(0, len(blade_angle_list))]
        graph4.line(x_a, blade_angle_list, color='blue', line_width=2)
        self.graph_list.append(graph4)
        #### end blade scalar product graph

        edge_theta_d = self.make_edge_theta_dict(solution)
        self.edge_theta_equation(edge_theta_d, solution)
        
    def make_graph_list(self):
        self.graph_list = []
        for solution in self.solution_list:
            self.make_graph(solution)
            self.make_projector_graph(solution)
           
    def make_transform_graph(self):
        self.transform_graph_list = []
        for solution in self.transform_list:
            self.make_graph(solution)

    def show_graphs(self):
        grid = gridplot(self.graph_list, ncols=self.num_cols, width=500, height=500)
        show(grid)

    def show_transform_graphs(self):
        grid = gridplot(self.transform_graph_list, ncols=2, width=500, height=500)
        show(grid)
        
    def solve(self, angles, *args):        
        self.simplex.rotate_simplex(angles, self.geometry.projected_dim)
        self.simplex.project_simplex(self.geometry.projected_dim)
        objective_value = self.objective_function(self.geometry.projected_dim)
        return objective_value 

    def search(self, num_vertices, projected_dim):
        self.N = num_vertices
        self.dimension = num_vertices - 1
        self.num_indep_angles = 2*self.dimension - 4
        self.bounds = self.calculate_bounds(self.dimension, projected_dim)
        self.geometry = Geometry(self.dimension, projected_dim)
        self.simplex = Simplex(self.geometry.vertex_len, self.geometry)
        angles = self.calculate_guess(projected_dim)
        result = optimize.minimize(self.solve, angles, method='SLSQP',
                                   bounds=self.bounds, constraints=(),
                          options={'maxiter': self.maxiter, 'ftol': 1e-08})      
        self.energy_val = self.energy(projected_dim)

        # Construct a solution candidate based on the outcome of the optimization.
        self.solution_candidate = Solution(self.N, self.energy_val, 0,
                                           self.simplex.simplex_rotated,
                                           angles,                                           
                                           self.get_projection(projected_dim),
                                           self.simplex.rotation.get_rotor(),
                                           self.objective_function.__name__,
                                           'SLSQP',
                                           projected_dim, self.geometry)
        
        self.solution_candidate.error = self.least_squares(self.solution_candidate)
        self.solution_candidate.result = result
        return self.solution_candidate

    def get_result(self):
        solution = self.solution_candidate
        print('solution_candidate.result object:\n')
        for k, v in zip(solution.result.keys(), solution.result.values()):
            print('{}: {}'.format(k, v))
        

    def store(self):
        self.solution_list.append(self.solution_candidate)
        print('solver: solution stored to solution_list.')
        
    def save(self, filename):
        pickle.dump(self.solution_list, open(filename, "wb"))
        print('solver: {} solutions written to {}.'.format(len(self.solution_list), filename))

    def load(self, filename):
        self.solution_list = []
        self.solution_list = pickle.load(open(filename, "rb"))
        print('solver: {} solutions loaded.'.format(len(self.solution_list)))

    def load_append(self, filename):
        new_solutions = pickle.load(open(filename, "rb"))
        for e in new_solutions:
            self.solution_list.append(e)
        print('solver: {} new solutions appended.'.format(len(new_solutions)))

    def pop(self):
        try:
            self.solution_list.pop(0)
        except:
            print('solver: solution_list is empty.')
        
class Simplex:

    def __init__(self, vertex_len, geometry):
        self.angles = None
        self.geometry = geometry
        self.dimension = geometry.dimension
        self.N = self.geometry.N
        self.basis_vectors = self.geometry.basis_vectors
        self.e1 = self.geometry.basis_vectors[0]
        self.e2 = self.geometry.basis_vectors[1]
        self.e12 = self.geometry.two_blades[0]
        self.rotation = Rotation(geometry)
        self.simplex_projected_1d = None
        self.simplex_projected_2d = None
        self.edges_rotated = None
        
        x = np.zeros([self.dimension, self.N])
        for k in range(0, self.dimension):
            s = 0.0
            for i in range(0, k):
                s = s + x[i, k] ** 2
            x[k, k] = np.sqrt(1.0 - s)

            for j in range(k + 1, self.N):
                s = 0.0
                for i in range(0, k):
                    s = s + x[i, k] * x[i, j]
                x[k, j] = (-1.0 / float(self.dimension) - s) / x[k, k]

        # Normalize vertex length to vertex_len and transpose.
        self.source_object = x.transpose()
        self.source_object = vertex_len * self.source_object
        self.object_rotated = self.source_object

        # Geometric Algebra conversions.
        self.simplex_vertices = self.convert_to_vector()
        self.simplex_rotated = self.simplex_vertices
        self.edges_rotated = self.edges(self.simplex_rotated)

    def get_projection(self, projected_dim):
        if projected_dim == 2:
            return self.simplex_projected_2d
        elif projected_dim == 1:
            return self.simplex_projected_1d
        
    def convert_to_vector(self):        
        vertices = []
        for vector in self.source_object:
            vertex = 0
            for e, i in zip(vector, range(self.dimension)):
                vertex += e ^ self.basis_vectors[i]
            vertices.append(vertex)
        return vertices

    def project_simplex(self, projected_dim):       
        """Project all simplex vertex vectors to 1-d or 2-d subspace."""
        if projected_dim == 1:
            self.simplex_projected_1d = [self.project_vector(vertex, 1) \
                                         for vertex in self.simplex_rotated]
            
        elif projected_dim == 2:
            self.simplex_projected_2d = [self.project_vector(vertex, 2) \
                                         for vertex in self.simplex_rotated]            

    def project_vector(self, vector, projected_dim):       
        """Project a vector onto a 1 or 2 d subspace."""
        if projected_dim == 1:
            return (vector.lc(self.e1)) * self.e1            
        elif projected_dim == 2:
            return (((vector | self.e1) * self.e1) + ((vector | self.e2) * self.e2))

    def reject_vector(self, vector, projected_dim):      
        """Calculate the 'rejection' of a vector with respect to a 1 or 2-d subspace."""
        if projected_dim == 1:
            return (vector ^ self.e1) * self.e1            
        elif projected_dim == 2:
            return ((vector ^ self.e12) * (self.e12.inv()))
        
    def kick(self, angle):
        # Rotate through all axes by 'angle' to make sure vertices do not
        # coincide in the initial orientation.
        
        blds = [v for v in self.geometry.two_blades]
        rotors = [np.e **((angle/2) * b) for b in blds]

        for r in rotors:
            self.rotate(r, 2)
                
    def print_simplex(self):        
        print(self.simplex_vertices)

    def wedge_sum(self, v_list):
        print('v_list: ', v_list)
        sum = 0
        for i in range(0, self.N):
            sum += self.reject_vector(v_list[i], 2) ^ self.project_vector(v_list[i], 2)
            print('sum: ', sum)
        print('wedge_sum: ', sum)
        
    def print_vertices(self):
        print('\nvertices:')
        s = 0
        for i in range(0, self.N):
            print ('vertex[{}]: {}'.format(i, self.simplex_rotated[i]))
            
    def log_sum(self):
        sum = 0
        for edge in self.edges_rotated:
            r = self.e1 * edge
            sum += self.rLog(r)
        return sum

    def edges(self, vertex_list):
        # Construct edges given a vertex list
        rij = []
        for i in range(0, self.N):
            for j in range(0, self.N):
                if j != i and i > j:
                    rij.append(vertex_list[i] - vertex_list[j])
        return rij

    def rLog(self, rotor):
        # Take the log of a rotor using simple calculation.
        if abs(rotor(2)) < kPSEUDO_ZERO:
            blade = 0
        else:
            blade = rotor(2)/abs(rotor(2))

        angle = np.arctan(abs(rotor(2))/abs(rotor(0)))
        return blade * angle

    def rotate_simplex(self, angles, projected_dim):
        self.angles = angles
        self.rotation.construct_rotor(angles)
        rotor = self.rotation.get_rotor()
        self.simplex_rotated = [rotor * vertex * (~rotor) \
                                for vertex in self.simplex_vertices]
        self.edges_rotated = self.edges(self.simplex_rotated)

    def rotate(self, rotor, projected_dim):            
        self.simplex_rotated = [rotor * vertex * (~rotor) \
                                for vertex in self.simplex_rotated]
        self.edges_rotated = self.edges(self.simplex_rotated)
        

class Rotation:
    
    def __init__(self, geometry):
        self.geometry = geometry
        self.e1 = self.geometry.basis_vectors[0]
        self.e2 = self.geometry.basis_vectors[1]        
        self.trace = False
        self.two_blades = self.geometry.two_blades
        self.rotation_blades_1d = self.rotation_blades1d()
        self.rotation_blades_2d = self.rotation_blades2d()
        self.rotor = 1
        self.theta_list = []

    def rotation_blades1d(self):
        blades = []
        for b in self.two_blades:
            if (self.e1 | b) != 0:
                blades.append(b)
        if self.trace:
            print('{} 1d rotation blades: {}'.format(len(blades), blades))
        return blades

    def rotation_blades2d(self):
        blades = []
        for b in self.two_blades:
            if (self.e1 | b) != 0 or (self.e2 | b) != 0:
                # equals 2*n-4 indep. angles.
                blades.append(b)
        if self.trace:        
            print('{} 2d rotation blades: {}'.format(len(blades), blades))
        return blades

    def construct_rotor(self, theta_list):
        rotor = 1
        self.theta_list = theta_list

        if self.geometry.projected_dim == 1:
            blade_list = self.rotation_blades_1d
            
        elif self.geometry.projected_dim == 2:
            blade_list = self.rotation_blades_2d
            
        for blade, theta in zip(blade_list, theta_list):
            angle_coeff = np.pi
            s_int = np.e ** ((angle_coeff * theta/2) * blade)
            rotor = s_int * rotor
        self.rotor = rotor

    def set_rotor(self, rotor):
        self.rotor = rotor

    def get_rotor(self):
        return self.rotor

    def print_rotor(self):
        print(self.rotor)

    def print_thetas(self):
        print(self.theta_list)
