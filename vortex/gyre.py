#!/usr/local/bin/python3.8
###########################################################################
# Gyre.py - 
# Generate exact solutions to the point vortex relative equilibrium problem.
# 
# Author: Phillip Robertson (c) 2020.
###########################################################################

import argparse
import clifford as cf
import numpy as np
import cmd
import matplotlib.pyplot as plt
import scipy as sp
from scipy import spatial
import sys

kTHRESHHOLD = 10**(-3)

class Gyre:

    def __init__(self, d):
        self.dimension = d
        self.N = self.dimension + 1        
        self.layout, self.blades = cf.Cl(self.dimension)
        self.length = self.dimension/2
        self.basis_vectors()
        self.dual_subspace = self.basis_vectors[0]
        self.projection_plane = self.basis_vectors[0]^self.basis_vectors[1]
        self.vertex_coincidence_map = {}

        x = np.zeros([self.dimension, self.N])

        for k in range(0, self.dimension):
            s = 0.0
            for i in range(0, k):
                s = s + x[i,k] ** 2
            x[k,k] = np.sqrt(1.0 - s)

            for j in range(k+1, self.N):
                s = 0.0
                for i in range(0, k):
                    s = s + x[i,k] * x[i,j]
                x[k,j] = (-1.0/float(self.dimension) - s) / x[k,k]

        self.proj_blade = self.basis_vectors[0]
        
        # Normalize vertex length to n/2
        self.source_object = x.transpose()
        k_scalar = self.dimension/2
        self.source_object = k_scalar * self.source_object             
        self.object_rotated = self.source_object
        
        # Generate vertices from coordinate coefficients. 
        self.vertices = []

        for i in range(0, self.N):
            self.vertex_coincidence_map[i] = 1
        
        for vector in self.source_object:
            vertex = 0
            zip(vector, range(self.dimension))
            for e, i in zip(vector, range(self.dimension)):
                vertex += e^self.basis_vectors[i]
            self.vertices.append(vertex)

        two_blades = []
        for b in self.blades.values():
            if b != 1 and b(2) != 0:             
                two_blades.append(b)

        self.two_blades = two_blades

        # Set initial orientation for the simplex.
        self.kick((np.pi/6))
                        
    def basis_vectors(self):
        b_v = []
        for i in range(1, self.dimension+1):
            b_v.append(self.blades[self.layout.names[i]])
        self.basis_vectors = b_v

    def centroid(self, vertices):
        s = 0
        i = 0
        for v in vertices:
            s += self.vertices[v]
            i += 1
        s = (1/i) * s
        return s

    def face_dual(self, vertices):      
        facet = 1
        for i in range(1, len(vertices)):
            facet = facet ^ (self.vertices[vertices[0]] - self.vertices[vertices[i]]) 
        return facet, facet.dual()
    
    def convert_to_components(self, vector, blade, d):
        blade_vectors = []
        for bv in self.basis_vectors:
            if bv^blade == 0:
                blade_vectors.append(bv)

        v = np.zeros([d])        
        for i in range (0, d):         
            v[i] = vector | blade_vectors[i] 
        return(v)

    def edge(self, pair):
        return(self.vertices[pair[0]] - self.vertices[pair[1]])

    def gale_transform(self, cv):        
        # Construct Gale transform of projected vertex set.
        
        gale = np.zeros((self.dimension-1, self.dimension + 1))
        
        t = []
        [t.append(1) for i in range(0, self.dimension + 1)]
        
        # Set top row to 1's.
        gale[0] = np.array(t)
        gale = np.transpose(gale)

        # read in components of projected vectors as columns. 
        for e, v in zip(gale, cv):
            for i in range(0, self.dimension-2):
                e[i+1] = v[i]                

        gale = np.transpose(gale)
        ns = sp.linalg.null_space(gale)
        
        # print('gale:\n{} '.format(gale))
        print('nullspace: \n{}'.format(ns))
        return(ns)

    def k_blades(self, k):
        # return blades of grade k
        blades = []
        for e in self.blades:
            if self.blades[e](k) != 0:
                blades.append(self.blades[e])
        return(blades)

    def projection(self, v, blade):
        return((v | blade) * blade.inv())

    def rejection(self, v, blade):
        return((v^blade) * blade.inv())
    
    def get_partitions(self, n):
        t = list(self.partition(n))
        return(t)

    def gyrate(self, coincidence):
        # Find rotations so vertices coincide in projection blades.
        
        for c in coincidence:
            self.print_vertices()
            if len(c) == 2:
                self.coincide_edge(c, [self.basis_vectors[0],
                                       self.basis_vectors[3],
                                       self.basis_vectors[6]])
            if len(c) == 3:
                self.coincide_face(c)
               
    def kick(self, angle):
        # Rotate through all axes by 'angle' to make sure vertices do not
        # coincide in the initial orientation. This is a convenience function.
        
        blds = [v for v in self.two_blades]
        spinors = [self.spinor(angle, b) for b in blds]

        vertices = self.vertices
        for s in spinors:
            vertices = [self.rotate(v, s) for v in vertices]
                
        self.vertices = vertices

    def morton(self, cv):
        morton_v_arr = []
        morton_v = np.zeros([2])
        distance_matrix = sp.spatial.distance.cdist(cv,
                                                    cv,
                                                    'sqeuclidean')
        indices = range(0, self.dimension+1)
        for index in indices:
            sx = 0.0
            sy = 0.0
            
            for i in indices:
                if i != index:
                    if distance_matrix[index][i] < kTHRESHHOLD:
                        # reject solution - do not plot morton points. 
                        return([])
                    else:
                        distance_squared = distance_matrix[index][i]

                    sx = sx + (cv[index][0] - cv[i][0])/distance_squared
                    sy = sy + (cv[index][1] - cv[i][1])/distance_squared

            morton_v_arr.append(np.array([sx, sy]))
        return(morton_v_arr)
            
       # morton_vectors = np.array(morton_v_arr)
            
    def normal_blade_cos(self, v, bld):
        # Find rotation blade and theta given a vector and a subspace dimension, k.
        
        projection = self.projection(v, bld)
        theta = np.arccos(abs(projection)/abs(v))
        b = v ^ projection
        b = b/abs(b)
        return((theta, b))

    def normal_blade_sin(self, v, bld):
        # Find rotation blade and theta given a vector and a subspace dimension, k.
        # TODO(pr): fix this to conform with other normal_blade() functions.
        
        projection = self.projection(v, bld)
        theta = np.arcsin(abs(projection)/abs(v))
        print('projection: {}\ntheta: {}'.format(projection, theta))
        a = (v ^ bld) * bld.inv()
        b = v ^ a
        b = b/abs(b)
        return((theta, b))
   
    def coincide_dual(self, face):
        # Find the dual of a 'face' object, which can be a line or a plane.
        
        subspace_vectors = self.basis_vectors[:-(len(face) - 1)]
        print('subspace_vectors; {} face: {}'.format(subspace_vectors, face))        
        for v in subspace_vectors:
            facet, facet_dual = self.face_dual(face)
            vp = self.projection(v, facet_dual)
            theta, n_blade = self.normal_blade_cos(vp, v)
            s = self.spinor(theta, n_blade)
            self.vertices = [self.rotate(v, s) for v in self.vertices]
            
            facet, facet_dual = self.face_dual(face)
            print('facet_dual: {}'.format(facet_dual))
            
        # rotate face so the centroid coincides with e1
        print('centroid: {} (centroid | facet): {}'.format(self.centroid(face),
                                                           self.centroid(face) | facet))
        
    def coincide_edge(self, edge, bv_list):

        for bv in bv_list:
            v = self.edge((edge[0], edge[1]))
            print('edge: {} vector: {}'.format(edge, v))
            theta, n_blade = self.normal_blade_sin(v, bv)
            print('theta: {}\nn_blade: {}'.format(theta, n_blade))
            s = self.spinor(theta, n_blade)
            self.vertices = [self.rotate(v, s) for v in self.vertices]        

    def coincide_face(self, face):
        # Rotate simplex until three points coincide on e1. 
        
        e1 = self.basis_vectors[0]
        
        # first rotation
        v1 = self.edge((face[0], face[1]))
        v2 = self.edge((face[0], face[2]))
        f_blade = v1 ^ v2

        theta, n_blade = self.normal_blade_sin(e1, f_blade)
        s = self.spinor(theta, n_blade)
        self.vertices = [self.rotate(v, s) for v in self.vertices]
        v1 = self.edge((face[0], face[1]))
        print('v1 . e1: {}'.format(self.projection(v1, e1)))
        
        # Second rotation to bring centroid to be normal with e1.        

        centroid = self.centroid(face)
        theta, n_blade = self.normal_blade_sin(centroid, e1)
        s = self.spinor(theta, n_blade)
        self.vertices = [self.rotate(v, s) for v in self.vertices]

    def plot_blades(self):
        max_cols = 6
        num_blades = len(self.two_blades)        
        
        if num_blades <= max_cols:
            total_rows = 1
        else:
            total_rows = num_blades // max_cols + 1

        # print('total_rows: ', total_rows)
        fig = plt.figure(figsize = (4*max_cols, 4*(total_rows)))
        fig.suptitle('N = '+str(self.N))
        fig.subplots_adjust(hspace=0.4, wspace=0.6)
        
        c = 1
        for b in self.two_blades:
            
            projection = self.project_simplex(b)
            cv = [self.convert_to_components(vector, b, 2) for vector in projection]
            x = [e[0] for e in cv]
            y = [e[1] for e in cv]
            mortons = self.morton(cv)
            
            mx =[m[0] for m in mortons]
            my =[m[1] for m in mortons]
            
            ax = fig.add_subplot(total_rows, max_cols, c, adjustable='box', aspect=1.0)
            
            ax.scatter(x, y, marker='o')
            ax.scatter(mx, my, marker='s')
            ax.set_title(str(b))
            c+=1
            
        plt.show()               

    def plot_projection(self, cv, dimension):       
        fig = plt.figure()
        
        ax1 = fig.add_subplot(111)
        ax1.set_xlabel('X')
        ax1.set_ylabel('Y')
        ax1.set_title('N = {}'.format(self.N))
                
        if dimension == 1:
            x = [e[0] for e in cv]
            y = [0 for e in cv]
            ax1.scatter(x, y, marker='o')
            plt.show()
            
        elif dimension == 2:
            x = [e[0] for e in cv]
            y = [e[1] for e in cv]
            ax1.scatter(x, y, marker='o')
            plt.show()
        else:
            return

    def print_vertices(self):
        print('vertices: ')
        for e in self.vertices:
            print(e)

    def print_source_object(self):
        for e in self.source_object:
            print(e)

    def project_simplex(self, blade):
        subspace_proj = [self.projection(v, blade) for v in self.vertices]
        self.subspace_proj = subspace_proj
        return subspace_proj
            
    def rotate(self, v, s):
        return(s*v*~s)   
       
    def spinor(self, theta, blade):
        t = np.e ** (-(theta/2)*blade) 
        return(t)
    
    def sum_vertices(self):
        s = 0
        for e in self.vertices:
            s = s + e
        print('vertex sum: {}'.format(abs(s)))
